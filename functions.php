<?php
add_theme_support( 'custom-logo' );
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

/*-------------------------------/
    Add Custom Post Types
/-------------------------------*/
function projectsTaxonomy_init() {
  register_taxonomy(
    'project_type',
    'Projects',
    array(

      'label' => __( 'Project Type' ),
      'rewrite' => array( 'slug' => 'project_type' ),
      'query_var' => true,
      'show_admin_column' => true,
      'show_in_rest' => true,
      'capabilities' => array(
        'assign_terms' => 'edit_posts',
        'edit_terms' => 'manage_categories',
        'mange_terms' => 'manage_categories',
        'delete_terms' => 'manage_categories'
        )
      )
    );
}
add_action( 'init', 'projectsTaxonomy_init' );

/*-------------------------------/
    Add Custom Post Types
/-------------------------------*/
function bkevans_custom_post_types (){

  $labels = array(
    'name' => 'Projects',
    'singular_name' => 'Project',
    'add_new' => 'Add New Project',
    'all_items' => 'All Projects',
    'add_new_item' => 'Add New Project',
    'edit_item' => 'Edit Project',
    'new_item' => 'New Project',
    'view_item' => 'View Project',
    'search_item' => 'Search Latest Projects',
    'not_found' => 'No items found',
    'not_found_in_trash' => 'No items found in trash',
    'parent_item_colon' => 'Parent Item'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'has_archive' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'show_in_rest' => true,
    'supports' => array('title','editor', 'thumbnail', 'excerpt', 'page-attributes'),
    'menu_icon' => 'dashicons-desktop',
    'taxonomies' => array('project_type'),
    'menu_position' => 4,
  );
  register_post_type('Projects',$args);

}

add_action('init', 'bkevans_custom_post_types');

/*-------------------------------/
    Custom titles for post types
/-------------------------------*/
function bkevans_change_title_text( $title ){

  $screen = get_current_screen();

  if  ( 'projects' == $screen->post_type ) {
    $title = "Enter the Project Name";
  }

  return $title;
  
} 
add_filter( 'enter_title_here', 'bkevans_change_title_text' );

/*-------------------------------/
    Add menu order to posts 
/-------------------------------*/
function bkevans_rest_menu_order(){

  register_rest_field(
    array('post', 'projects'),
    'menu_order',
    array(
      'get_callback' => function($post) {
        return get_post_field('menu_order', $post->id);
      }
    )
  );
  
} 
add_action( 'rest_api_init', 'bkevans_rest_menu_order' );

?>
